package pl.margielewski.androidbindingpoc.application;

import android.annotation.SuppressLint;
import android.app.Application;

import timber.log.Timber;

/**
 * Created by margielewski on 27.06.2016.
 */
public class ProjectApplication extends Application {

    @SuppressLint("MissingSuperCall")
    @Override
    public void onCreate() {
        initLogger();
    }

    private void initLogger() {
        Timber.plant(new Timber.DebugTree());
    }
}
