package pl.margielewski.androidbindingpoc.feature.listadapterbinding.viewmodel;

import android.databinding.ObservableField;

import pl.margielewski.androidbindingpoc.commonmodel.DataModel;

/**
 * Created by margielewski on 27.05.2016.
 */
public class ListViewAndroidBindingViewModel {

    public final DataModel model;

    public ListViewAndroidBindingViewModel(final DataModel model) {
        this.model = model;
        this.initalize();
    }

    private final ObservableField<String> title = new ObservableField<>();


    private void initalize() {
        getTitle().set("Witam w mojej pierwszej aplikacji z bindingiem");
    }

    public ObservableField<String> getTitle() {
        return title;
    }
}
