package pl.margielewski.androidbindingpoc.feature.singledata.datasource;

import pl.margielewski.androidbindingpoc.architecture.annotations.ServiceData;

/**
 * Created by margielewski on 27.05.2016.
 */
public interface ISingleDataService {

    @ServiceData(ServiceName = "getActivityTitle")
    String getTitle();
}
