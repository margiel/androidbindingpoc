package pl.margielewski.androidbindingpoc.feature.swipelayoutlist.viewmodel;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.swipe.adapters.BaseSwipeAdapter;

import java.util.List;

import pl.margielewski.androidbindingpoc.R;
import pl.margielewski.androidbindingpoc.commonmodel.DataManager;
import pl.margielewski.androidbindingpoc.commonmodel.DataModel;
import pl.margielewski.androidbindingpoc.databinding.ListviewAndroidbindingSwipelayoutItemBinding;

public class ListViewAndroidBindingSwipeLayoutAdapter extends BaseSwipeAdapter {

    private Context mContext;
    private List<DataModel> dataList;

    public ListViewAndroidBindingSwipeLayoutAdapter(Context mContext) {
        this.mContext = mContext;
        dataList = DataManager.getDataModelList();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        final DataModel model = getModel(position);

        ListviewAndroidbindingSwipelayoutItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.listview_androidbinding_swipelayout_item, parent, false);
        View convertView = binding.getRoot();

        binding.setViewModel(new ListViewAndroidBindingSwipeLayoutViewModel(getModel(position)));
        convertView.setTag(binding);

        return convertView;

        /*View v = LayoutInflater.from(mContext).inflate(R.layout.listview_swipelayout_item, null);
        SwipeLayout swipeLayout = (SwipeLayout)v.findViewById(getSwipeLayoutResourceId(position));
        swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

                Timber.d(String.format("onStartOpen %s", model.getId()));
            }

            @Override
            public void onOpen(SwipeLayout layout) {

                Timber.d(String.format("onOpen %s", model.getId()));
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

                Timber.d(String.format("onStartClose %s", model.getId()));
            }

            @Override
            public void onClose(SwipeLayout layout) {

                Timber.d(String.format("onClose %s", model.getId()));
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
            @Override
            public void onDoubleClick(SwipeLayout layout, boolean surface) {
                Toast.makeText(mContext, "DoubleClick", Toast.LENGTH_SHORT).show();
            }
        });
        v.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "click delete", Toast.LENGTH_SHORT).show();
            }
        });
        return v;*/
    }

    @Override
    public void fillValues(int position, View convertView) {
        /*DataModel model = getModel(position);

        TextView t = (TextView)convertView.findViewById(R.id.position);
        t.setText(String.valueOf(model.getId()));

        t = (TextView)convertView.findViewById(R.id.text_data);
        t.setText(model.getTitle());*/
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    private DataModel getModel(int position) {
        return (DataModel) getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
