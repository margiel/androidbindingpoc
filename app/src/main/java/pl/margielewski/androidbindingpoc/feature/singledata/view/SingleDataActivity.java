package pl.margielewski.androidbindingpoc.feature.singledata.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import pl.margielewski.androidbindingpoc.R;
import pl.margielewski.androidbindingpoc.databinding.SingledataActivityBinding;
import pl.margielewski.androidbindingpoc.feature.singledata.viewmodel.SingleDataViewModel;

/**
 * Created by margielewski on 27.05.2016.
 */
public class SingleDataActivity extends AppCompatActivity {

    private SingleDataViewModel viewModel = new SingleDataViewModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.singledata_activity);

        SingledataActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.singledata_activity);
        binding.setViewModel(viewModel);
    }
}
