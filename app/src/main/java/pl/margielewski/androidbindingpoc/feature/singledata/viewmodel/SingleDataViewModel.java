package pl.margielewski.androidbindingpoc.feature.singledata.viewmodel;

import android.databinding.ObservableField;

/**
 * Created by margielewski on 27.05.2016.
 */
public class SingleDataViewModel {

    private final ObservableField<String> title = new ObservableField<>();

    public SingleDataViewModel() {
        this.initalize();
    }

    private void initalize() {
        getTitle().set("Witam w mojej pierwszej aplikacji z bindingiem");
    }

    public ObservableField<String> getTitle() {
        return title;
    }
}
