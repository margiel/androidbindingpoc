package pl.margielewski.androidbindingpoc.feature.listadapterbinding.viewmodel;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import pl.margielewski.androidbindingpoc.R;
import pl.margielewski.androidbindingpoc.commonmodel.DataManager;
import pl.margielewski.androidbindingpoc.commonmodel.DataModel;
import pl.margielewski.androidbindingpoc.databinding.ListviewadapterItemBinding;

public class ListViewAndroidBindingAdapter extends BaseAdapter {

    private Context mContext;
    private List<DataModel> dataList;

    public ListViewAndroidBindingAdapter(Context mContext) {
        this.mContext = mContext;
        dataList = DataManager.getDataModelList();
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }


    public DataModel getModel(int position) {
        return (DataModel) getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListviewadapterItemBinding binding = null;
        if (convertView == null) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.listviewadapter_item, parent, false);
            convertView = binding.getRoot();
        } else {
            binding = (ListviewadapterItemBinding) convertView.getTag();
        }

        binding.setViewModel(new ListViewAndroidBindingViewModel(getModel(position)));
        convertView.setTag(binding);

        return convertView;
    }

}