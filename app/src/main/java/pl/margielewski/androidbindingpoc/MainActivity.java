package pl.margielewski.androidbindingpoc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import pl.margielewski.androidbindingpoc.feature.listadapterbinding.view.ListViewAndroidBindingActivity;
import pl.margielewski.androidbindingpoc.feature.singledata.view.SingleDataActivity;
import pl.margielewski.androidbindingpoc.feature.swipelayoutlist.view.ListViewAndroidBindingSwipeLayoutActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openListViewAndroidBindingSwipeLayoutActivity(View view) {
        Intent intent = new Intent(this, ListViewAndroidBindingSwipeLayoutActivity.class);
        startActivity(intent);
    }

    public void openNextActivity(View view) {
        Intent intent = new Intent(this, SingleDataActivity.class);
        startActivity(intent);
    }

    public void openListViewAndroidBindingActivity(View view) {
        Intent intent = new Intent(this, ListViewAndroidBindingActivity.class);
        startActivity(intent);
    }


}
