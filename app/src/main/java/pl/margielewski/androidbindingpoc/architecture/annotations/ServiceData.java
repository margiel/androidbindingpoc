package pl.margielewski.androidbindingpoc.architecture.annotations;

/**
 * Created by margielewski on 27.05.2016.
 */
public @interface ServiceData {
    String ServiceName() default "";
}
