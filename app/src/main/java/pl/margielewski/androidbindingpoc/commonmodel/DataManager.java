package pl.margielewski.androidbindingpoc.commonmodel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by margielewski on 27.06.2016.
 */
public class DataManager {

    public static List<DataModel> getDataModelList(){
        ArrayList<DataModel> list = new ArrayList<>();

        for (int i = 0; i < 50; i++){
            DataModel item = new DataModel();
            item.setId(i);
            item.setTitle(String.format("Title %s", i));
            list.add(item);
        }
        return list;
    }
}
