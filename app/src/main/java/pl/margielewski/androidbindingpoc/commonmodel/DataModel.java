package pl.margielewski.androidbindingpoc.commonmodel;

/**
 * Created by margielewski on 27.06.2016.
 */

public class DataModel {
    private int id;
    private String title;

    public int getId() {
        return id;
    }

    public String getStringId() {
        return String.valueOf(id);
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
